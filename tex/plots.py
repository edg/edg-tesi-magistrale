import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Using seaborn's style
# plt.style.use('seaborn')
# With LaTex fonts
# plt.style.use('tex')

width = 345
sns.set_theme(style="whitegrid")
df = pd.read_csv("../variable_area_results.csv")
df.x_length /= 1000
df.y_length /= 1000

sns.scatterplot(data=df, x="x_length", y="y_length", s=300)
plt.xlabel("x_length")
plt.ylabel("y_length")
plt.savefig("images/dgvDistributionLatLon.pdf", format='pdf', bbox_inches='tight')
plt.cla()

sns.histplot(df.x_length / df.y_length)
plt.xlabel("ratio")
plt.savefig("images/dgvDistributionRatio.pdf", format="pdf", bbox_inches="tight")
plt.cla()

sns.histplot(df.x_length * df.y_length)
plt.xlabel("area")
plt.savefig("images/dgvDistributionArea.pdf", format="pdf", bbox_inches="tight")
plt.cla()

df = pd.read_csv("../fixed_area_results.csv")
df["diff_1_2"] = df.time1 - df.time2
sns.histplot(data=df, x="diff_1_2")
plt.xlabel("time1 - time2 (s)")
plt.savefig("images/dgfTime1MinusTime2.pdf", format="pdf", bbox_inches="tight")
plt.cla()

def po(df):
    return 59.8 * (20/df.grid_step) * (df.ni*df.nj/403776) * (864/df.n_procs) * (1 - 0.3*df.single_precision)

df["estimated_duration_2"] = po(df)
df["est_1_abserr"] = (df.time2 - df.estimated_duration).abs()
df["est_2_abserr"] = (df.time2 - df.estimated_duration_2).abs()

df = pd.read_csv("../variable_area_results.csv")

df["estimated_duration_2"] = po(df)
df["est_1_abserr"] = (df.time2 - df.estimated_duration).abs()
df["est_2_abserr"] = (df.time2 - df.estimated_duration_2).abs()

sns.kdeplot(data=df, x="est_1_abserr", log_scale=True, cumulative=True, cut=0, label="PP")
sns.kdeplot(data=df, x="est_2_abserr", log_scale=True, cumulative=True, cut=0, label="PO")
plt.xlabel("Absolute error (s)")
plt.legend()
plt.savefig("images/dgvPpPoTime2AbsErrCdf.pdf", format="pdf", bbox_inches="tight")
plt.cla()

df["est_1_relerr"] = (df.time2 - df.estimated_duration).abs()/df.time2
df["est_2_relerr"] = (df.time2 - df.estimated_duration_2).abs()/df.time2
sns.kdeplot(data=df, x="est_1_relerr", log_scale=True, cumulative=True, cut=0, label="PP")
sns.kdeplot(data=df, x="est_2_relerr", log_scale=True, cumulative=True, cut=0, label="PO")
plt.xlabel("Relative error")
plt.legend()
plt.savefig("images/dgvPpPoTime2RelErrCdf.pdf", format="pdf", bbox_inches="tight")
plt.cla()

plt.plot([df.time1.min(), df.time1.max()], [df.time1.min(), df.time1.max()])
sns.scatterplot(data=df, x="time1", y="estimated_duration", legend="full")
plt.xlabel("Actual value (s)")
plt.ylabel("Predicted value (s)")
plt.savefig("images/dgvPpTime1ScatterTargetPrediction.pdf", format="pdf", bbox_inches="tight")
plt.cla()

plt.plot([df.time2.min(), df.time2.max()], [df.time2.min(), df.time2.max()])
sns.scatterplot(data=df, x="time2", y="estimated_duration", legend="full")
plt.xlabel("Actual value (s)")
plt.ylabel("Predicted value (s)")
plt.savefig("images/dgvPpTime2ScatterTargetPrediction.pdf", format="pdf", bbox_inches="tight")
plt.cla()

sd_grp_keys = ["x_length", "y_length", "grid_step", "n_nodes", "n_cores", "single_precision"]
sd_grp_df = df.groupby(sd_grp_keys).filter(lambda d: len(d) > 1).groupby(sd_grp_keys).time2.agg(["count", "min", "max", "mean", "std"]).sort_values("std").reset_index()
sns.histplot(sd_grp_df["std"]/sd_grp_df["mean"], bins=50, stat="density")
plt.savefig("images/dgvSpRstdDistribution.pdf", format="pdf", bbox_inches="tight")
plt.cla()

plt.plot(list(range(-11, 11)), [x if x > 0 else 0 for x in range(-11, 11)])
plt.savefig("images/relu.pdf", format="pdf", bbox_inches="tight")
plt.cla()

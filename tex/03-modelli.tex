\chapter{Modelli per la previsione del tempo di esecuzione}

In questo capitolo, verranno presentati i modelli presi in considerazione per la
previsione del tempo di esecuzione di COSMO-Model e gli strumenti usati per
l'implementazione.

Prima di tutto, possiamo affermare che il problema in esame è un esempio di
regressione, che consiste nella ricerca di una relazione tra una variabile di
cui si vuole predire il valore (detta variabile dipendente) e una o più
variabili (dette indipendenti), le quali influenzano la risposta. Nel caso del
presente lavoro, le variabili indipendenti sono quelle definite nelle tabelle
\ref{tab:DGFParams} e \ref{tab:DGVParams}, rispettivamente per il dataset DGF e
DGV. La variabile indipendente è il tempo di esecuzione che, sulla base delle
considerazioni del capitolo 2, è possibile individuare nei tempi per la
generazione della prima e seconda ora di previsione.

La relazione tra la variabile dipendente $Y$ e le variabili indipendenti $X_1,
X_2, \dots, X_n$ può essere approssimata dall'equazione:

\begin{equation}
    \label{eq:regressionEquation}
    Y = f(X_1, X_2, \dots, X_n) + \varepsilon
\end{equation}

dove $\varepsilon$ rappresenta la discrepanza nell'approssimazione.

La risoluzione di problemi di regressione è uno degli ambiti di interesse del
machine learning, un ramo dell'intelligenza artificiale che si occupa dello
studio di algoritmi e tecniche che sono in grado di apprendere dai dati a
disposizione. Nel caso in esame, si parla in particolare di apprendimento
supervisionato, in quanto abbiamo a disposizione un insieme di dati (training
set) in cui ad ogni input è associato l'output desiderato: in questi casi, gli
algoritmi sono addestrati in modo da trovare una approssimazione della
relazione generica quale \ref{eq:regressionEquation}. Tipicamente, lo scopo
finale di un modello di machine learning è quello di poter essere in grado di
effettuare previsioni sufficientemente corrette anche per dati che non ha mai
visto: questa caratteristica è detta generalizzazione e viene normalmente valutata
usando un insieme di dati non usato in fase di addestramento, detto test set.

La qualità di un modello è dunque data da quanto riesce a minimizzare l'errore
$\varepsilon$ di \ref{eq:regressionEquation}, che può essere espresso in due
modi:

\begin{itemize}
    \item Training error: l'errore che si ottiene in fase di addestramento del
        modello sulla base del training set. Scopo dell'addestramento è proprio
        la sua minimizzazione.
    \item Generalization error: l'errore che si ottiene facendo valutare un
        input mai visto ad un modello addestrato, misura quindi la capacità di
        generalizzazione.
\end{itemize}

La qualità di un modello deve trovare un compromesso tra questi due aspetti.
Infatti, un training error basso può essere sintomo di una scarsa capacità di
generalizzazione: in questo caso si parla di overfitting, vale a dire che il
modello si è adattato troppo ai dati del traning set e di conseguenza la
differenza tra il training error e il generalization error è troppo alto. Al
contrario, nel caso in cui il training error sia troppo alto, si parla di
underfitting: il modello non è dunque in grado di catturare la complessità
della relazione tra variabili indipendenti e dipendenti.

Oltre alla stima presentata nel capitolo 2, denominata Previsione Ottimistica (PO),
sono stati selezionati due modelli di machine learning per la previsione del
tempo di esecuzione:

\begin{itemize}
    \item Regressione lineare \cite{chatterjee_regression_2006}
    \item Rete neurale feedfoward \cite{russel2010}
\end{itemize}

L'uso di più modelli si basa sul principio di economia (o rasoio di di Occam):
nella scelta di un modello per la risoluzione di un problema è consigliabile
usare quello che, a parità di risultato, è più semplice. Nel caso dei modelli
scelti, la regressione lineare è decisamente più semplice rispetto alla rete
neurale, ma al tempo stesso ha una capacità minore di rappresentare relazioni
complesse rispetto ad una rete neurale e di conseguenza potrebbe avere
prestazioni peggiori. Si rende dunque necessario confrontare la qualità delle
previsioni dei due modelli in modo da valutare se la maggiore complessità della
rete neurale è giustificabile.

\section{Regressione lineare}

Uno dei più semplici e comuni modelli per predire un valore a partire da un
insieme di variabili è quello della regressione lineare. Nel caso in esame,
parliamo di regressione lineare multipla poiché il valore da predire dipende da
più di un parametro.

Come suggerisce il nome, la relazione tra la variabile dipendente $Y$ e le
variabili indipendenti $X_1, X_2, \dots, X_n$ è lineare e quindi l'equazione
\ref{eq:regressionEquation} diventa:

\begin{equation}
    \label{eq:multipleLinearRegression}
    Y = \beta_0 + \displaystyle\sum_{i=1}^{n} \beta_i X_i + \varepsilon
\end{equation}

dove $\beta_i$ sono i coefficienti di regressione e $\varepsilon$ è l'errore
statistico. La vera relazione tra $Y$ e $X = X_1, \dots, X_n$ viene dunque
approssimata con una funzione lineare e la discrepanza è rappresentata
dell'errore statistico, che non contiene ulteriori informazioni su $Y$ che non
siano già state catturate da $X$.

L'uso della regressione lineare prevede di trovare i coefficienti $\beta_0,
\beta_1, \dots, \beta_n$ che meglio approssimano la vera relazione tra $Y$ e
$X$ basandosi sulle osservazioni possedute. La soluzione si basa sul metodo dei
minimi quadrati, che consiste nel trovare i coefficienti tali che la distanza
euclidea complessiva tra i valori osservati e i valori predetti (quindi la
somma dei quadrati di tutti gli errori statistici, da cui il nome) è minima.

Per ogni dato $y_i, x_{i1}, x_{i2}, \dots, x_{in}$ ($i = 1, \dots, p$) vale
l'equazione \ref{eq:multipleLinearRegression}, che può essere riscritta come

\begin{equation}
    \varepsilon_i = y_i - \beta_0 - \beta_1 x_{i1} - \dots - \beta_n x_{in}
\end{equation}

La distanza euclidea complessiva tra i valori osservati e quelli predetti è
data da:

\begin{equation}
    S(\beta_0, \beta_1, \dots, \beta_n) = \displaystyle\sum_{i=1}^{p} \varepsilon^2  = \displaystyle\sum_{i=1}^{p} (y_i - \beta_0 - \beta_1 x_{i1} - \dots - \beta_n x_{in} )^2
\end{equation}

E dunque si ottiene un problema di minimizzazione la cui soluzione sono i pesi ottimi ${\widehat{\beta}} = [\widehat{\beta}_0, \dots, \widehat{\beta}_n]$:

\begin{equation}
    \widehat{\beta} = \arg \min_{\beta_0, \dots, \beta_n} S(\beta_0, \dots, \beta_n)
\end{equation}

Che viene risolto ponendo a zero le derivate parziali di $S$ rispetto a $\beta_i$:

\begin{equation}
    \frac{\partial S}{\partial \beta_i} = 0
\end{equation}

Sia $\mathbf{X}$ la matrice $p \times n$ dei $p$ dati di input (con $x_{1i} =
1$) e $\mathbf{y}$ il vettore dei valori attesi, la soluzione è pari a:

\begin{equation}
    \widehat{\beta} = (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X} \mathbf{y}
\end{equation}

Appare evidente come questo modello abbia il vantaggio di essere semplice e
computazionalmente efficiente. Tuttavia, l'assunzione di linearità potrebbe non
riuscire a rappresentare con sufficiente precisione la complessità di una
relazione.

\section{Rete neurale feedforward}
La rete neurale feedforward è un modello basato su neuroni artificiali, che
sono ispirati all'ambito della neuroscienza e che costituiscono le unità
elementari da cui è composta la rete.

Il neurone artificiale è composto da uno o più ingressi $x_i$ che sono
combinati linearmente tra loro (usando dei pesi associati $w_i$) e il cui
risultato è poi fornito come ingresso per una funzione di attivazione $f$, la
cui uscita rappresenta quella complessiva del neurone (equazione \ref{eq:neuron}).

\begin{equation}
    y = f(\displaystyle\sum_{i=1}^{n} w_i x_i)
    \label{eq:neuron}
\end{equation}

Le funzioni di attivazione a disposizione sono molteplici, ma nel presente
lavoro prendiamo in considerazione solo la rectified linear unit (ReLU), che è
probabilmente quella di uso più comune, la cui equazione è:

\begin{equation}
    f(x) = \max(x, 0)
\end{equation}

Questa funzione di attivazione è molto semplice e computazionalmente poco
onerosa. Inoltre, non essendo limitata per valori positivi, non soffre del
problema del vanishing gradient, cioè la tendenza del gradiente a diminuire
drasticamente all'aumentare della profondità della rete: questo comporta il
rischio di avere cambiamenti minimi negli strati più vicini all'input. Infine,
la scelta è stata influenzata anche dall'uso di ReLU nel modello dello studio
di Pittino et al. \cite{10.1145/3324989.3325720}.

Il termine feedforward indica l'aspetto monodirezionale del modello, in cui il
dato di ingresso fluisce lungo i vari componenti fino ad ottenere un risultato,
senza che siano presenti retroazioni. I neuroni sono organizzati a strati,
ognuno dei quali è connesso al successivo; in figura
\ref{fig:reteNeuraleFeedforward} è mostrato un esempio di questa architettura,
in cui possiamo identificare un primo strato di ingresso (input layer) che
riceve lo stimolo dall'esterno, seguito da uno o più strati intermedi (hidden
layer) e infine un ultimo strato (output layer) che fornisce il risultato
complessivo della rete. Nel caso in cui vi siano due o più hidden layer, si
parla di deep network. Le connessioni sono realizzate collegando l'output delle
unità di uno strato all'input di tutte quelle successive (fully connected)
\cite{Goodfellow-et-al-2016}.

\begin{figure}[tb]
    \begin{center}
        \includegraphics[width=0.7\textwidth]{images/nn.dot.pdf}
    \end{center}
    \caption{Rete neurale feedforward composta da quattro layer}
    \label{fig:reteNeuraleFeedforward}
\end{figure}

La risposta di una rete dipende dai pesi delle unità che la compongono ed è
quindi necessario trovare dei valori che permettano di ottenere una
approssimazione sufficientemente precisa della relazione che si vuole
modellare. Il processo di correzione dei pesi per la minimizzazione dell'errore
viene detto apprendimento. Esistono diversi paradigmi, nel presente lavoro
viene considerato solo l'apprendimento supervisionato, che si basa
sull'utilizzo di un insieme di dati (training set) ognuno dei quali è composto
da un ingresso e dal corrispondente risultato e grazie ai quali è possibile
applicare aggiustamenti ai pesi confrontando l'uscita del modello con il valore
desiderato e cercando di minimizzare l'errore.

Il metodo più diffuso per l'aggiornamento dei pesi è l'algoritmo di
retropropagazione dell'errore (backpropagation), un procedimento composto da
due passi: nella fase in avanti (forward propagation) la rete viene alimentata
con il training set e si raccolgono i risultati, che sono poi confrontati con i
valori desiderati applicando una funzione di costo (scelta in base alla
tipologia del problema), la quale rappresenta l'errore di approssimazione.
Nella fase all'indietro (backpropagation step) viene calcolato il gradiente
della funzione di costo rispetto ai pesi valutando uno strato alla volta in
direzione opposta al passo precedente (quindi partendo dall'output layer fino
all'input layer) e calcolando quindi quanto ogni peso ha contribuito
all'errore. I pesi vengono poi aggiornati mediante un algoritmo di
ottimizzazione, che nella sua forma più semplice è il metodo della discesa del
gradiente (gradient descent), il quale consiste nell'usare l'opposto del
gradiente moltiplicato per uno scalare (detto learning rate) alla ricerca di un
minimo della funzione di costo. L'equazione \ref{eq:gradientDescent} mostra
come il nuovo peso $\widehat{w}_{ij}$ sia ottenuto dalla somma dell'attuale
valore $w_{ij}$ e della derivata parziale dell'errore $J(\mathbf{w})$ (dove
$\mathbf{w}$ è il vettore dei pesi) moltiplicata per il learning rate $\mu$.

\begin{equation}
    \widehat{w}_{ij} = w_{ij} - \mu \frac{\partial J(\mathbf{w})}{\partial w_{ij}}
    \label{eq:gradientDescent}
\end{equation}

L'ottimizzazione della funzione di costo è ripetuta fino a che non si ottiene
un errore accettabile oppure fino ad un limite massimo di training epoch, cioè
il numero di volte che il training set è stato risottomesso. L'idea di fondo è
quindi quella di avvicinarsi ad un minimo della funzione di costo usando come
passo $\mu$ volte il gradiente. Si può intuire come il valore del learning rate
sia critico: se è troppo grande si rischia di non poter mai raggiungere il
minimo, se invece è troppo piccolo il processo di apprendimento richiede troppe
iterazioni.

Vi sono metodi di ottimizzazione alternativi al gradient descent, ma nel
presente lavoro è stato considerato come alternativa solo Adam
\cite{kingma2017adam}, un algoritmo molto utilizzato nell'ambito del deep
learning per le buone prestazioni dimostrate empiricamente. La sostanziale
differenza rispetto al gradient descent consiste nel fatto che il learning rate
non è fisso ma varia in funzione del gradiente e dei pesi della rete.

In entrambi i casi, il calcolo della funzione di costo e conseguentemente del
gradiente nonché l'aggiornamento dei pesi viene effettuato dopo aver alimentato
la rete con un certo numero di dati (batch size) alla volta, che può essere
pari a 1 (stochastic gradient descent) o alla dimensione del training set
(batch gradient descent) o a un valore intermedio (minibatch gradient descent).
Il batch size può influenzare l'apprendimento: un alto valore comporta una
maggiore parallelizzazione delle elaborazioni, ma comporta anche una
convergenza meno rapida poiché il numero di aggiornamenti per epoch è pari al
rapporto tra dimensione del training set e batch size. Inoltre, l'uso di un
sottoinsieme di dati ad ogni aggiornamento introduce un rumore che può
permettere di uscire da minimi locali o punti di sella della funzione di costo
\cite{DBLP:journals/corr/abs-1206-5533}.

Nel processo di addestramento della rete, tuttavia, si può incorrere nel
problema dell'overfitting: in questo caso il modello si è adattato troppo ai
dati del training set, perdendo così la sua abilità di generalizzare, cioè di
fornire una risposta precisa a fronte di ingressi non ancora visti. Esistono
diverse tecniche per mitigare questo problema: tra queste, ricordiamo solo
quelle utilizzate nel presente lavoro. Una prima strategia consiste nel
penalizzare la funzione di costo con il prodotto tra uno scalare (il weight
decay) e il quadrato dei pesi, in modo da spingere il modello a minimizzare
questi ultimi: infatti, alti valori dei pesi sono generalmente sintomo di una
rete complessa, instabile ed eccessivamente specializzata sul training set,
laddove è preferibile invece un modello più semplice. L'equazione
\ref{eq:weightDecay} mostra la nuova funzione di costo $\widehat{J}(w)$ in cui
viene aggiunto il prodotto tra weight decay $\lambda$ e quadrato dei pesi.

\begin{equation}
    \widehat{J}(w) = J(w) + \lambda w^T w
    \label{eq:weightDecay}
\end{equation}

Un'altra strategia considerata è quella del dropout, che consiste nel
disattivare con una certa probabilità alcuni neuroni delle rete ad ogni
training epoch: in fase di addestramento è possibile si formino dei percorsi
privilegiati nella rete dovuti al fatto che una unità modifichi il suo peso in
modo da correggere errori di altre unità successive, generando così dei
coadattamenti che potrebbero penalizzare la capacità di generalizzazione della
rete. Il dropout nasce dall'ipotesi che lo spegnimento casuale dei neuroni
renda difficile la generazione di queste relazioni di coadattamento (figura
\ref{fig:dropout}) \cite{JMLR:v15:srivastava14a}.

\begin{figure}[tb]
    \begin{center}
        \includegraphics[width=0.6\textwidth]{images/dropout.dot.pdf}
    \end{center}
    \caption{Tre esempi di possibili applicazioni del dropout ad una rete: i
    nodi in grigio sono quelli disattivati}
    \label{fig:dropout}
\end{figure}

Dalle caratteristiche delle reti neurali presentate fino ad ora si può notare
come i pesi non siano gli unici parametri che possono influenzare il
comportamento di una rete: l'opportuna configurazione di elementi come il
numero di layer, il numero di unità per layer, il learning rate, etc.
contribuiscono in modo significativo a migliorarne le prestazioni. Questi sono
detti iperaparametri (hyperparameter) e sono appunto quei parametri che
influiscono sull'apprendimento e non è dunque possibile (o è molto complesso)
ottimizzarli durante l'apprendimento stesso. Una volta identificati una serie
di iperparametri, è dunque necessario effettuare l'hyperparameter tuning, cioè
trovare dei valori tali che il modello abbia delle prestazioni soddisfacenti.
Per poter risolvere questo problema, è necessario selezionare una metrica con
cui valutare il modello (non necessariamente uguale alla funzione di costo
usata nella fase di addestramento del modello) e una modalità di esplorazione
dello spazio dei parametri.

La ricerca degli iperparametri ottimi equivale a risolvere il problema

\begin{equation}
    \widehat{x} = \arg \min_{x \in \mathcal{X}} J(x)
\end{equation}

dove $J(x)$ è la funzione che rappresenta la metrica scelta per la valutazione
delle prestazioni del modello, $\mathcal{X}$ è lo spazio degli iperparametri e
$\widehat{x}$ rappresenta i valori ottimi degli iperparametri rispetto a $J(x)$.
Non sapendo quale sia l'andamento di $J(x)$, è necessario esplorare
$\mathcal{X}$ alla ricerca di una soluzione.

La modalità di ricerca di una buona soluzione è critica, in quanto ogni
iterazione comporta l'addestramento del modello e la sua valutazione. La
ricerca più semplice che si possa fare è quella manuale, che ovviamente ha il
grosso limite di essere poco efficiente. I due algoritmi di ricerca
tradizionali sono:

\begin{itemize}
    \item Grid search: esplora tutto lo spazio degli iperparametri
        selezionati. Questa ricerca ha il vantaggio di essere esaustiva ma
        è anche onerosa, in quanto il suo costo cresce in maniera esponenziale
        con il numero degli iperparametri.
    \item Random search: esplora lo spazio in modo casuale e non esaustivo. È
        ovviamente meno oneroso di grid search e può ottenere buoni risultati
        in tempi molto più rapidi, soprattutto se una buona parte degli
        iperparametri non influenza fortemente le prestazioni del modello.
\end{itemize}

Per il presente lavoro è stato scelto un algoritmo alternativo, cioè Bayesian
Optimization, una modalità di ricerca che si basa sulla costruzione di un
modello probabilistico per definire la metrica che ad ogni iterazione $i$ viene
aggiornato sulla base del valore $f(x^{i})$ e infine usato per scegliere il
punto $x^{i+1}$ che viene reputato più promettente. Ad ogni passo, quindi,
l'algoritmo migliora il modello che approssima la funzione di costo e fa dunque
ipotesi (che all'inizio saranno necessariamente casuali) sempre più precise sul
percorso di esplorazione della funzione di costo. Rispetto ai precedenti
metodi, il vantaggio di Bayesian Optimization consiste nel fatto di procedere
in maniera informata, cioè tenendo conto dei valori già considerati, e di
conseguenza riesce a trovare buone soluzioni con un numero minore di tentativi.

La valutazione della metrica si basa generalmente su un validation set, un
sottoinsieme del training set che non viene sottomesso al modello in fase di
addestramento ma solo al suo termine. Tuttavia, questo comporta che la stima
della metrica è dipendente da come è stato scelto il validation set e dunque
diverse ripartizioni potrebbero dare diversi risultati. Una soluzione
frequentemente utilizzata è offerta dal k-fold cross-validation, un metodo che
consiste nel generare $k$ sottoinsiemi del training set per effettuare $k$
volte la valutazione della metrica usando, ad ogni iterazione, un diverso
sottoinsieme come validation set e i rimanenti $k-1$ come training set. Con
questo metodo sono generati $k$ modelli identici, ognuno dei quali però
addestrato e valutato con un insieme di dati diverso. La valutazione
complessiva viene poi calcolata dalla media delle $k$ metriche. Usando questo
metodo è possibile ottenere una rappresentazione più accurata delle prestazioni
del modello rispetto all'uso di una sola coppia di training e validation set
\cite{russel2010}.

% \begin{figure}[tb]
%     \begin{center}
%         \includegraphics[height=0.3\textwidth]{images/kfold.pdf}
%     \end{center}
%     \caption{Schema di k-fold cross validation (k=5). I fold colorati sono i validation set di ogni iterazione}
%     \label{fig:reteNeuraleFeedforward}
% \end{figure}

\section{Metriche per la valutazione dei modelli}

Basandosi sull'equazione \ref{eq:regressionEquation} si può affermare che, nel
caso di regressioni, la valutazione delle prestazioni di un modello è
necessariamente funzione dell'errore $\varepsilon$.

Il Mean Square Error (MSE), cioè l'errore quadratico medio, che
equivale alla media del quadrato delle differenze tra valore atteso $y$ e
previsto $\widehat{y}$.

\begin{equation}
    \mathit{MSE} =
    \frac{1}{n}\displaystyle\sum_{i=1}^{n} (\widehat{y}_i - y_i)^2 =
    \frac{1}{n} \displaystyle\sum_{i=1}^{n} \varepsilon_i^2
\end{equation}

L'elevamento al quadrato ha come effetto di rimuovere il segno - e quindi il
valore risulta sempre positivo. Un ulteriore effetto è quello di evidenziare
errori eccessivi e penalizzare i modelli in cui si verificano.

Il Mean Absolute Error (MAE) è una metrica più semplice, che stabilisce un
rapporto lineare tra la metrica e l'errore riscontrato.

\begin{equation}
    \mathit{MAE} =
    \frac{1}{n}\displaystyle\sum_{i=1}^{n} \lvert\widehat{y}_i - y_i\rvert =
    \frac{1}{n}\displaystyle\sum_{i=1}^{n} \lvert\varepsilon_i\rvert
\end{equation}

Il Mean Absolute Percentage Error (MAPE) rappresenta l'errore relativo
(espresso tra $0$ e $1$ o come percentuale). Rispetto alle due precedenti
metriche, ha il vantaggio di normalizzare l'errore rispetto al valore atteso
e quindi è possibile confrontare previsioni su scala diversa. Tuttavia, per
valori molti prossimi allo zero, può presentare valori molto alti e dunque
influenzare la media in modo fuorviante.

\begin{equation}
    \mathit{MAPE} =
    \frac{1}{n}\displaystyle\sum_{i=1}^{n} \lvert\frac{\widehat{y} - y}{y}\rvert =
    \frac{1}{n}\displaystyle\sum_{i=1}^{n} \lvert\frac{\varepsilon_i}{y}\rvert
\end{equation}

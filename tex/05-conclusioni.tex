\chapter{Conclusioni e sviluppi futuri}

Il presente lavoro aveva come scopo quello di verificare l'utilizzo di modelli
deep learning per la previsione dei tempi di esecuzione del modello numerico di
previsione meteorologica COSMO-Model in uso presso la Struttura
Idro-Meteo-Clima di Arpae Emilia-Romagna ed eseguito su GALILEO, sistema HPC di
CINECA.

L'attività ha richiesto l'individuazione di un insieme di parametri
caratteristici che influenzano il tempo di esecuzione per ogni configurazione
di COSMO-Model e i loro range. Per questa attività ci si è avvalsi della
consulenza di un esperto del dominio.

In seguito, sono stati eseguite circa 10 mila configurazioni diverse di
COSMO-Model, divise in due tipologie: nella prima, è stata usata un'area
geografica fissa e le varie combinazioni dei parametri sono state esplorate in
maniera densa, mentre nella seconda, più sparsa, sono state fatte variare anche
le dimensioni dell'area. Tutti gli esperimenti sono stati eseguiti calcolando
solo la prima e la seconda ora di previsione (rispettivamente, \texttt{time1} e
\texttt{time2}) e registrando il tempo necessario per ognuna di esse. Le ore
di previsione successive alla seconda non sono state eseguite in quanto
richiedono un tempo uguale a \texttt{time2}, mentre \texttt{time1} richiede un
tempo lievemente maggiore. In questo modo, si è potuto ottimizzare il tempo di calcolo
a disposizione per l'esecuzione dei job e la raccolta dei dati. Questi dati
sono stati raccolti in due dataset, uno per ogni tipologia di griglia, detti
DGF (griglia fissa) e DGV (variabile). A partire da questi due sono stati
inoltre generati, usando una semplice formula, due dataset sintetici che
contengono i tempi complessivi di esecuzione (\texttt{time\_tot}) delle varie
configurazioni di COSMO-Model per un numero di ore variabile. Ogni dataset
sintetico è 139 volte il dataset originale.

Infine, sono stati effettuati alcuni esperimenti in cui sono stati confrontati
un modello di regressione lineare (LR), una rete neurale con configurazione non
ottimizzata (FCNN-noopt), una rete neurale con configurazione ottimizzata per
l'esperimento (FCNN-opt) e una semplice equazione parametrica basata sulla
versione operativa di COSMO-Model (PO) e usata durante la selezione degli
esperimenti per filtrare quelli troppo lunghi (e quindi poco significativi). In
tabella \ref{tab:ElencoEsperimenti} l'elenco complessivo degli esperimenti.

\begin{table}[ht]
    \centering
    \begin{tabularx}{\textwidth}{ | l | X | X | }
        \hline
        Nome        & Descrizione & Modelli \\
        \hline
        DGF-PT1     & Previsione \texttt{time1} su griglia fissa & LR, FCNN-noopt e FCNN-opt \\
        DGF-PT2     & Previsione \texttt{time2} su griglia fissa & PO, LR, FCNN-noopt e FCNN-opt \\
        DGF-PTTOT   & Previsione \texttt{time\_tot} su griglia fissa & LR e FCNN-opt \\
        DGF-PTTOT-S & Previsione \texttt{time\_tot} su griglia fissa con hyperparameter tuning fatto su sottoinsieme del training set & FCNN-opt \\
        DGF-PT1     & Previsione \texttt{time1} su griglia variabile & LR, FCNN-noopt e FCNN-opt \\
        DGF-PT2     & Previsione \texttt{time2} su griglia variabile & PO, LR, FCNN-noopt e FCNN-opt \\
        DGF-PTTOT   & Previsione \texttt{time\_tot} su griglia variabile & LR e FCNN-opt \\
        DGF-PTTOT-S & Previsione \texttt{time\_tot} su griglia variabile con hyperparameter tuning fatto su sottoinsieme del training set & FCNN-opt \\
        DGF-PT1-V   & Previsione \texttt{time1} su griglia variabile & FCNN-opt di DGF-PT1 con preprocessamento dell'input \\
        DGF-PT2-V   & Previsione \texttt{time2} su griglia variabile & FCNN-opt di DGF-PT2 con preprocessamento dell'input \\
        DGV-PT1-PT2 & Previsione di \texttt{time\_tot} su griglia variabile & Combinazione lineare dei modelli FCNN-opt di DGV-PT1 e DGV-PT2 \\
        \hline
    \end{tabularx}
    \caption{}
    \label{tab:ElencoEsperimenti}
\end{table}

I modelli bastati su reti neurali con ottimizzazione degli iperparametri si
sono rivelati quelli con le prestazioni migliori in tutti gli esperimenti. Al
contrario, PO e LR si sono rivelate stime poco precise. In particolare, si è
verificato in tutti gli esperimenti che PO tende a sottostimare i job più
lunghi, mentre LR tende a sovrastimarli. Queste due tipologie di modelli non
riescono a catturare le relazioni non lineari che esistono tra i parametri di
input e i tempi di esecuzione. Al contrario, l'uso di una rete neurale ha
introdotto un miglioramento che, nel caso dei modelli con iperparametri
ottimizzati, può arrivare fino a un ordine di grandezza per quanto riguarda le tre metriche
considerate (MAE, RMSE e MAPE, si veda tabella
\ref{tab:RiassuntoMetricheModelli1}). I modelli FCNN-opt, oltre ad un'alta
precisione, presentano la caratteristica di avere un errore relativo
particolarmente contenuto soprattutto nel caso dei job più lunghi, come è
possibile notare dai grafici di correlazione tra errore relativo e tempo di
esecuzione osservato (si veda figure \ref{fig:RiassuntoOsbRelErr}). Questa
proprietà è desiderabile in quanto la precisione della previsione è
particolarmente importante per i job che occupano per più tempo le
risorse, che sono evidentemente quelli più critici per una corretta allocazione
delle risorse stesse. Questo effetto è stato ottenuto utilizzando RMSE come funzione
di costo da minimizzare nella ricerca degli iperparametri ottimi, effettuata
con Bayesian Optimization. Inoltre, si è verificata la capacità dei modelli di
individuare correttamente delle anomalie nei tempi di esecuzione dei job
basandosi sul discostamento dell'errore assoluto e/o relativo della previsione
rispetto alle normali prestazioni.

\begin{table}[ht]
    \centering
    \begin{tabular}{ | l | r | r | r | r | r | r | }
        \hline
                & \multicolumn{3}{c|}{DGF-PT2} & \multicolumn{3}{c|}{DGV-PT2} \\
        \hline
                & PO    &    LR & FCNN-opt & PO    & LR    & FCNN-opt \\
        \hline
        MAE     & 7.31  &  3.4  & 0.58     & 9.8   & 10.22 & 0.98 \\
        RMSE    & 13.27 &  6.67 & 1.79     & 21.58 & 25.35 & 2.9  \\
        MAPE    & 28.30 & 25.67 & 2.84     & 27.10 & 38.84 & 3.77 \\
        \hline
    \end{tabular}
    \caption{Metriche dei modelli di DGF-PT2 e DGV-PT2}
    \label{tab:RiassuntoMetricheModelli1}
\end{table}

\begin{figure}[tb]
    \begin{center}
        \subfigure[DGF-PT2]{
            \includegraphics[width=0.4\textwidth]{images/dgf-pt2/fcnn-opt-correlazione-osservazione-errore-relativo.pdf}
        }
        \subfigure[DGV-PT2]{
            \includegraphics[width=0.4\textwidth]{images/dgv-pt2/fcnn-opt-correlazione-osservazione-errore-relativo.pdf}
        }
    \end{center}
    \caption{Correlazione tra \texttt{time2} e errore relativo}
    \label{fig:RiassuntoOsbRelErr}
\end{figure}

Una volta appurata la maggiore precisione dei modelli deep learning con
iperparametri ottimizzati, si è proceduto a considerare con più attenzione lo
scenario della previsione su griglia variabile, che è lo scenario più
significativo. In questo ambito, si è appurato che:

\begin{itemize}
    \item Le previsioni sul tempo totale effettuate con il modello di DGV-PTTOT
        sono, in termini di errore relativo, poco migliori rispetto a quelle
        del modello di DGV-PT1-PT2 (combinazione lineare dei modelli di DGV-PT1
        e DGV-PT2). Tuttavia, l'addestramento e ottimizzazione degli
        iperparametri di DGV-PTTOT è molto più onerosa in termini di tempo e
        risorse (tabella \ref{tab:RiassuntoMetricheModelli2}).
    \item Le previsioni sul tempo totale effettuate col modello DGV-PTTOT-S,
        i cui iperparametri sono stati ottimizzati con un sottoinsieme del
        dataset usato per DGV-PTTOT, sono poco meno precise rispetto a quest'ultimo
        ma richiedono un tempo di addestramento sensibilmente minore (tabella
        \ref{tab:RiassuntoMetricheModelli2}).
    \item Le previsioni di \texttt{time1} e \texttt{time2} su griglia variabile
        posso essere fatte con i modelli di DGF-PT1-V e DGF-PT1-V, cioè i modelli
        DGF-PT1 e DGF-PT2 a cui viene aggiunto a monte un preprocessamento sul
        numero di nodi. Una minore precisione rispetto a DGV-PT1 e DGV-PT2
        (tabella \ref{tab:RiassuntoMetricheModelli3}) è compensata da un onere
        minore nella generazione di un dataset su griglia fissa per
        l'addestramento rispetto ad uno su griglia variabile.
\end{itemize}

\begin{table}[ht]
    \centering
    \begin{tabular}{ | l | r | r | r | }
        \hline
                & DGV-PTTOT & DGV-PT1-PT2 & DGV-PTTOT-S \\
        \hline
        MAE     & 52.58     & 62.19         & 64.92       \\
        RMSE    & 162.66    & 193.22        & 184.76      \\
        MAPE    & 3.34      & 3.44          & 4.21        \\
        Tempo hyperparameter tuning & 10d6h & 7d19h & 1d22h \\
        Numero iterazioni   & 100   & 500   & 1000 \\
        Numero processi   & 10    & 2     & 2 \\
        \hline
    \end{tabular}
    \caption{Metriche dei modelli FCNN-opt per la previsione di
    \texttt{time\_tot} su griglia variabile. Il numero di processi è relativo a quelli usati durante l'hyperparameter tuning.}
    \label{tab:RiassuntoMetricheModelli2}
\end{table}

\begin{table}[ht]
    \centering
    \begin{tabular}{ | l | r | r || r | r | }
        \hline
                & DGV-PT1 & DGF-PT1-V & DGV-PT2 & DGF-PT2-V \\
        \hline
        MAE     & 2.29    & 5.01      & 0.98    & 1.37      \\
        RMSE    & 4.35    & 8.04      & 2.9     & 2.68      \\
        MAPE    & 10.27   & 20.73     & 3.77    & 7.84      \\
        \hline
    \end{tabular}
    \caption{Metriche dei modelli FCNN-opt per la previsione di \texttt{time1}
    e \texttt{time2} su griglia variabile}
    \label{tab:RiassuntoMetricheModelli3}
\end{table}

Il modello DGV-PT1-PT2 è quello che meglio riesce a conciliare precisione e
tempi contenuti di hyperparameter tuning. Infatti, pur avendo delle prestazioni
molto simili a quelle di DGV-PTTOT, allo stesso tempo usa un numero molto più
basso di risorse per l'hyperparameter tuning. Dalle prestazioni di DGV-PTTOT-S
si può inoltre ipotizzare che, all'aumentare dei dati raccolti, si possa
effettuare un nuovo tuning usando solo un sottoinsieme del nuovo dataset, in
modo da limitare l'uso di risorse di calcolo.

I modelli DGF-PT1-V e DGF-PT1-V possono essere invece una alternativa
"economica" nel caso in cui lo scopo principale sia quello di avere previsioni
su un'area fissa e nel caso in cui sia invece accettabile che le eventuali
previsioni su griglie diverse (in termini di area e passo di griglia) siano
meno precise.

Questo lavoro rappresenta solo una esplorazione dell'efficacia dei modelli deep
learning per la previsione dei tempi di esecuzione di COSMO-Model. Infatti, ci
si è concentrati sul suo utilizzo presso Arpae-SIMC e sulla sua esecuzione sul
sistema HPC GALILEO. L'approccio può però essere esteso ad altri ambiti di
applicazione di COSMO-Model (ad esempio, quello climatologico), così come la
sua esecuzione su macchine diverse da GALILEO, in modo da ottenere un modello
di previsione del tempo di esecuzione che possa essere il più generico
possibile pur mantenendo i livelli di accuratezza di quelli implementati nel
presente lavoro.

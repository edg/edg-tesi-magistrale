\chapter{Creazione dei dataset}

Non essendo disponibile uno storico delle esecuzioni del modello, parte del
lavoro è stato dedicato alla sua creazione. Sulla base delle indicazioni di un
esperto del settore, Davide Cesari dell'Unità Modellistica di Arpae-SIMC, sono
state individuate le tipologie di dataset, i parametri da considerare e i
valori di questi ultimi.

\section{Definizione dei dataset e dei parametri}

Come mostrato nel capitolo precedente, la sostanziale differenza tra le
configurazioni di COSMO riguarda la dimensione e il passo della griglia. A
questi due parametri si possono aggiungere il numero di nodi e il numero di
core per nodo utilizzati. Dal prodotto di questi ultimi si ottiene il numero di
processi, ognuno dei quali si occupa di effettuare i calcoli su un singolo
sottodominio: a seconda di come sono suddivisi gli assi $x$ e $y$ del dominio, i
sottodomini possono avere un diverso rapporto tra larghezza e altezza. L'ultimo
parametro di interesse è l'esecuzione in singola o doppia precisione.

Sono state individuate due tipologie di dataset:

\begin{itemize}
    \item Dataset a griglia fissa $10.0 \times 10.0$ gradi (DGF).
    \item Dataset a griglia variabile (DGV).
\end{itemize}

Le due tipologie hanno i medesimi parametri, a meno della dimensione della
griglia nel caso di DGF: questo permette di ottenere un dataset denso, in cui
lo spazio viene esplorato in modo più esaustivo. DGF nasce dall'interesse
dell'Unità Modellistica di avere a disposizione un dataset fitto in cui
analizzare le prestazioni del modello in funzione dei cinque parametri
definiti (tale studio esula dagli obiettivi del presente lavoro).

\begin{table}[ht]
    \centering
    \begin{tabularx}{\textwidth}{ | X | X | X | }
        \hline
        Nome del parametro & Descrizione del parametro & Valori \\
        \hline
        \texttt{grid\_step}        & Dimensione della cella (gradi)  & $0.02, 0.05, 0.08, 0.1$  \\
        \texttt{n\_nodes}          & Numero di nodi                  & $[10, 60]$ \\
        \texttt{n\_cores}          & Numero di core per nodo         & $[14, 32]$ (pari) \\
        \texttt{single\_precision} & Uso della singola precisione    & boolean \\
        \texttt{subdomain\_ratio}  & Rapporto tra $x$ e $y$ del sottodominio & $[1.0, 5.0]$ \\
        \hline
    \end{tabularx}
    \caption{Parametri del dataset a griglia fissa (DGF)}
    \label{tab:DGFParams}
\end{table}

\begin{table}[ht]
    \centering
    \begin{tabularx}{\textwidth}{ | X | X | X | }
        \hline
        Nome del parametro & Descrizione del parametro & Valori \\
        \hline
        \texttt{x\_length}         & Dimensione griglia asse $x$ (gradi) & $[7.0, 48.0]$ \\
        \texttt{y\_length}         & Dimensione griglia asse $x$ (gradi) $y$ & $[7.0, 25.0]$ \\
        \texttt{grid\_step}        & Dimensione della cella (gradi)  & $0.02, 0.05, 0.08, 0.1$  \\
        \texttt{n\_nodes}          & Numero di nodi                  & $[10, 60]$ \\
        \texttt{n\_cores}          & Numero di core per nodo         & $[14, 32]$ (pari) \\
        \texttt{single\_precision} & Uso della singola precisione    & boolean \\
        \texttt{subdomain\_ratio}  & Rapporto tra $x$ e $y$ del sottodominio & $[1.0, 5.0]$ \\
        \hline
    \end{tabularx}
    \caption{Parametri del dataset a griglia variabile (DGV)}
    \label{tab:DGVParams}
\end{table}

Le tabelle \ref{tab:DGFParams} e \ref{tab:DGVParams} mostrano i parametri e i
range utilizzati, su cui si possono fare alcune considerazioni.

Il valore del passo di griglia (\texttt{grid\_step}) deve essere un divisore
dei due lati della griglia: sono stati quindi presi in considerazione, tra i
valori compatibili con la griglia fissa, quelli di maggiore interesse. Gli
stessi valori sono stati utilizzati per DGV in modo da avere due dataset
omogenei e poter riutilizzare i dati di DGF in DGV.

Il range del numero dei nodi (\texttt{n\_nodes}) e del numero di core per nodo
(\texttt{n\_cores}) è stato individuato tra le configurazioni normalmente usate
e rispettando la quantità di risorse di calcolo disponibili per Arpae-SIMC. Il
numero di core, inoltre, è sempre pari, e i core utilizzati sono equamente
ripartiti tra i due socket.

Il rapporto tra $x$ e $y$ del sottodominio (\texttt{subdomain\_ratio}) è stato
limitato al solo intervallo che, realisticamente, viene utilizzato: come
precedentemente detto, la forma più allungata lungo $x$ comporta migliori
prestazioni, ma un rapporto troppo sbilanciato comporta poi un aumento dello
scambio dei messaggi tra processi.

Gli estremi inferiori dei domini lungo gli assi $x$ (\texttt{x\_length}) e $y$
(\texttt{y\_length}) sono stati scelti basandosi su un valore minimo per
ottenere una previsione significativa. Per quanto riguarda l'estremo superiore,
si è vincolati alla disponibilità dei dati iniziali con cui innescare
l'esecuzione del modello, che Arpae-SIMC possiede solo per una specifica area
(quella del modello operativo COSMO-5M, si veda figura \ref{fig:CosmoDomain}).
Ogni esperimento con griglia variabile è quindi limitato in questo dominio,
vale a dire un'area che copre all'incirca tutto il Mediterraneo. Nel sistema
ruotato in uso, la longitudine è nel range $[-25, 23]$ e la latitudine in
$[-13, 12]$: quindi l'area massima è di $48 \times 25$ gradi. È necessario
tuttavia ridurre lievemente la dimensione dell'area, in quanto il programma che
combina i dati estende lievemente l'area: si è quindi considerato il range
$[-24.8, 22.8]$, $[-12.9, 11.9]$, a cui corrisponde un'area di $47.6 \times
24.8$ gradi. Infine, nella scelta dei valori (\texttt{x\_length}) e $y$
(\texttt{y\_length}) si è considerato che i due parametri devono essere dei
multipli dei passi di griglia scelti.

Si può notare come tra i parametri non sia presente il numero di ore di
previsione: l'elaborazione ad ogni ora si basa sui dati dell'istante precedente
ed è dunque sequenziale. L'elaborazione di ogni ora di previsione richiede
sempre lo stesso tempo, ad eccezione della prima che ha una durata
significativamente più alta: nel caso della configurazione operativa di
COSMO-2I è circa il 20\% in più. Per limitare le risorse dedicate
all'esperimento si è dunque deciso di eseguire ogni configurazione del modello
solo per le prime due ore di previsione, rispettivamente \texttt{time1} e
\texttt{time2}, che rappresentano i due valori osservati da predire. Il tempo
totale di esecuzione di un modello per \texttt{N} ore di previsione è dunque
calcolabile con la seguente formula:

\begin{equation}
    \label{eq:time_tot}
    \mathit{time}_\mathit{tot} = \mathit{time}_1 + (N-1) \times \mathit{time}_2
\end{equation}

\section{Selezione delle configurazioni del modello}

A partire dai valori selezionati, è possibile enumerare tutte le configurazioni
del modello che possono essere generate per entrambi i dataset: il numero dei
passi di griglia è finito e, per ogni coppia di numero di nodi e numero di
core per nodo, esiste un numero finito di possibili suddivisioni in sottodomini.

Per poter pianificare gli esperimenti, è necessario operare una selezione: in
particolare, le configurazioni di un modello che richiedono un tempo eccessivo
possono essere scartate perché difficilmente saranno mai usate e
allungherebbero inutilmente i tempi di raccolta dei dati. La soglia scelta è di
500 secondi per ora di previsione: è un valore molto alto per le tempistiche
richieste a regime (ad esempio, COSMO-5M sarebbe completato in circa 10 ore) ma
necessario per includere le configurazioni relative ai recuperi delle
previsioni di giorni passati, che non necessitano di tempistiche strette. Per
stimare il tempo di esecuzione, è stata usata la formula
\ref{eq:pessimisticEvaluation}, che si basa su una proporzione rispetto al
modello operativo COSMO-2I.
\begin{equation}
    \label{eq:pessimisticEvaluation}
    \mathit{time} = \mathit{time_\mathit{C}}
                    \times \frac{\mathit{step_\mathit{C}}}{\mathit{step}}
                    \times \frac{\mathit{area}}{\mathit{area_\mathit{C}}}
                    \times \frac{\mathit{nproc_\mathit{C}}}{\mathit{nproc}}
                    \times (1 - 0.3 \times \mathit{sp})
\end{equation}

Dove
\begin{itemize}
    \item $\mathit{time_\mathit{C}}$ è il tempo di esecuzione medio della prima
        ora di previsione di COSMO-2I.
    \item $\mathit{step_\mathit{C}}$ e $\mathit{step}$ sono i passi di griglia
        rispettivamente di COSMO-2I e della configurazione in esame
        (\texttt{grid\_step}).
    \item $\mathit{area_\mathit{C}}$ e $\mathit{area}$ sono le aree
        rispettivamente di COSMO-2I e della configurazione in esame (prodotto
        di \texttt{x\_length} e \texttt{y\_length}).
    \item $\mathit{nproc_\mathit{C}}$ e $\mathit{nproc}$ sono il numero di
        processi rispettivamente di COSMO-2I e della configurazione in esame
        (prodotto di \texttt{n\_nodes} e \texttt{n\_cores}).
    \item $\mathit{sp}$ vale 1 se la configurazione in esame è in singola
        precisione, altrimenti 0: sperimentalmente, si è verificato che la
        singola precisione riduce di circa il 30\% il tempo di esecuzione.
        Corrisponde al parametro \texttt{single\_precision}.
\end{itemize}

Si può notare come la stima non tenga conto di \texttt{subdomain\_ratio}, che
comunque dovrebbe essere il parametro più trascurabile.

Inoltre, si evidenzia come sia stato scelto il tempo di esecuzione medio della
prima ora di previsione che, come precedentemente affermato, ha un durata
maggiore rispetto ai successivi: non sapendo in anticipo se la formula
sottostima o meno, si è preferito fare una valutazione pessimistica. Per
comodità, ci riferiremo da qui in avanti alla formula
\ref{eq:pessimisticEvaluation} come Previsione Pessimistica (PP).


Nel caso di DGF il numero di configurazioni possibili è pari a 5758 e, poiché
hanno tutte un tempo stimato minore di 500 secondi, nessuna è stata scartata.
Nel caso di DGV, invece, solo 892 su 42384 ($\sim 2\%$) sono oltre la soglia.

I due parametri aggiuntivi di DGV (\texttt{x\_length} e \texttt{y\_length})
richiedono un campionamento: sono stati quindi individuati 5 domini diversi
(uno dei quali è quello di DGF, cioè $10.0 \times 10.0$ gradi) le cui
dimensioni sono state scelte cercando di mantenere una distribuzione uniforme
in termini di coppie di valori (figura \ref{fig:dgvDistributionLatLon}),
rapporto tra le due (figura \ref{fig:dgvDistributionRatio}) e area (figura
\ref{fig:dgvDistributionArea}): questa distribuzione è stata scelta per cercare
di non sovrarappresentare alcuna configurazione.

\begin{figure}[tb]
    \begin{center}
        \includegraphics[width=0.4\textwidth]{images/dgvDistributionLatLon.pdf}
    \end{center}
    \caption{Dimensioni dei domini selezionati per DGV}
    \label{fig:dgvDistributionLatLon}
\end{figure}

\begin{figure}[tb]
    \centering
    \includegraphics[width=0.4\textwidth]{images/dgvDistributionRatio.pdf}
    \caption{Rapporto tra \texttt{x\_length} e \texttt{y\_length} dei domini
    selezionati per DGV}
    \label{fig:dgvDistributionRatio}
\end{figure}

\begin{figure}[tb]
    \centering
    \includegraphics[width=0.4\textwidth]{images/dgvDistributionArea.pdf}
    \caption{Area dei domini selezionati per DGV}
    \label{fig:dgvDistributionArea}
\end{figure}

Per ognuno dei cinque domini sono state selezionate casualmente 1000
configurazioni, per un totale di 5000 elementi. Il campionamento casuale è
stato scelto per rispettare la distribuzione dei vari parametri all'interno di
ogni dominio.

\section{Esecuzione delle configurazioni del modello e raccolta dati}

Le varie configurazioni sono state eseguite su GALILEO, nel medesimo ambiente
delle versioni operative di Arpae-SIMC e usando i medesimi strumenti. In
particolare, sono stati implementati una serie di script in Python per generare
ogni istanza del modello a partire dalla configurazione del modello operativo
COSMO-2I e modificando solo i campi relativi ai parametri di input scelti.
Anche per la schedulazione delle varie istanze del modello è stato usato il
medesimo strumento, vale a dire Simple Linux Utility Resource Management
(SLURM) \cite{10.1007/10968987_3}, un popolare sistema open source per la
gestione delle risorse cluster. Questo strumento è stato utile non solo per
replicare esattamente l'ambiente originale, ma anche per evitare di sovrapporsi
agli orari di attività quotidiana del modello operativo ed eseguire gli
esperimenti solo nelle finestre temporali di inattività: questo punto è stato
affrontato con particolare attenzione, in quanto eventuali errori avrebbero
potuto ritardare la produzione dell'output del modello, con effetti a cascata
su tutta la catena previsionale.

Ogni configurazione è stata eseguita in modalità esclusiva (opzione
\texttt{-{}-exclusive} di SLURM), quindi senza condividere il nodo con altri job.
Al contrario, i tempi di esecuzione sarebbero stati falsati in quanto, come già
affermato nel primo capitolo, il numero di core per nodo influenza le
prestazioni del modello. Di conseguenza, si deve essere certi dell'effettivo
carico di ogni nodo.

Al termine di ogni esecuzione, viene automaticamente generato il file
\texttt{YUTIMING} che elenca i tempi delle varie parti del modello, tra cui
quelli richiesti per la generazione di ogni ora di previsione (\texttt{time1} e
\texttt{time2}). È stato implementato un semplice parser in Python per
raccogliere queste informazioni in due file CSV, uno per tipologia di dataset
(DGF e DGV).

Poiché l'errore su \texttt{time2} influisce maggiormente sull'accuratezza della
previsione complessiva (si veda l'equazione \ref{eq:time_tot}) e poiché PP
tende generalmente a sovrastimare (si veda figura
\ref{fig:dgvPpScatterTargetPrediction}), è stata presa in considerazione una
seconda stima, identica a PP tranne che per la sostituzione del tempo medio
$\mathit{time_C}$ (equazione \ref{eq:pessimisticEvaluation}) della prima ora di
previsione di COSMO-2I con la seconda, inferiore del 20\% circa. Per
differenziarla da PP e poiché il secondo tempo di esecuzione è minore del
primo, viene chiamata Previsione Ottimistica (PO). Confrontando le funzioni
cumulate dell'errore assoluto e relativo nella previsione di \texttt{time2} da
parte PP e PO su DGV (figura \ref{fig:dgvPpPoTime2ErrCdf}), possiamo vedere
come PO sia più accurato e in particolare come tutte le previsioni abbiano un
errore relativo minore del 100\%, rispetto al 140\% di PP. Le medesime
considerazioni valgono per DGF, dove il comportamento di PP e PO è analogo.

\begin{figure}[tb]
    \begin{center}
        \subfigure[\texttt{time1}]{
            \label{fig:dgfPpTime1ScatterTargetPrediction}
            \includegraphics[width=0.4\textwidth]{images/dgvPpTime1ScatterTargetPrediction.pdf}
        }
        \subfigure[\texttt{time2}]{
            \label{fig:dgfPpTime2ScatterTargetPrediction}
            \includegraphics[width=0.4\textwidth]{images/dgvPpTime2ScatterTargetPrediction.pdf}
        }
    \end{center}
    \caption{correlazione tra tempo osservato e previsto da PP su DGV}
    \label{fig:dgvPpScatterTargetPrediction}
\end{figure}

\begin{figure}[tb]
    \begin{center}
        \subfigure[Errore assoluto]{
            \label{fig:dgvPpPoAbsErrCdf}
            \includegraphics[width=0.4\textwidth]{images/dgvPpPoTime2AbsErrCdf.pdf}
        }
        \subfigure[Errore relativo]{
            \label{fig:dgvPpPoRelErrCdf}
            \includegraphics[width=0.4\textwidth]{images/dgvPpPoTime2RelErrCdf.pdf}
        }
    \end{center}
    \caption{CDF dell'errore assoluto e relativo di PP e PO nella previsione di
    \texttt{time2} di DGF}
    \label{fig:dgvPpPoTime2ErrCdf}
\end{figure}

Per quanto riguarda DGV, tutti i tempi osservati (sia \texttt{time1} che a
maggior ragione \texttt{time2}) sono sotto la soglia dei 500 secondi,
rispettivamente 418.45 e 383.19 secondi.

Analizzando il dataset DGV, inoltre, è evidente come il rapporto tra le
dimensioni del sottodominio (parametro \texttt{subdomain\_ratio}) abbia
un'influenza limitata sul tempo di esecuzione. Il coefficiente di variazione di
\texttt{time2} per ogni gruppo di configurazioni che differiscono solo per
\texttt{subdomain\_ratio} ha come valore medio 2.3\% ed è al massimo il 10\%
nel 98\% dei casi.

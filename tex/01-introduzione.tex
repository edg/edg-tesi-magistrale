\chapter{Introduzione}

Il presente lavoro riguarda lo studio di tecniche per la previsione del tempo
di esecuzione del modello meteorologico COSMO nelle configurazioni gestite
dalla Struttura Idro-Meteo-Clima (SIMC) di Arpae ed eseguite su GALILEO,
sistema High Processing Computing (HPC) di CINECA. I modelli numerici di
previsione meteorologica sono applicazioni scientifiche che simulano
l'evoluzione del tempo atmosferico e sono tipicamente usati per la produzione
delle previsioni meteorologiche, una o anche più volte al giorno. Data la loro
complessità computazionale, ci si avvale generalmente di elaboratori dotati di
grandi capacità di calcolo, come appunto i sistemi HPC.

Di norma, i modelli sono eseguiti in una configurazione fissa e con tempi di
esecuzione definiti. Tuttavia, una accurata previsione del tempo di esecuzione
permette una migliore assegnazione delle risorse a disposizione, una più facile
individuazione della configurazione ottimale e una rapida ed efficace
riallocazione a fronte di variazioni delle risorse a disposizione, ad esempio
in seguito ad una diminuzione del numero di nodi a causa di guasti, per
l'introduzione di nuovi task o per la riesecuzione di istanze precedenti in
seguito a malfunzionamenti. Inoltre, in caso di scostamenti eccessivi dal tempo
osservato, il predittore permette di individuare anomalie sorte durante
l'esecuzione del modello.

Il lavoro di Pittino et al. \cite{10.1145/3324989.3325720} nell'ambito delle
scienze dei materiali ha mostrato come modelli Deep Learning permettano di
ottenere risultati molto accurati per la previsione dei tempi di esecuzione di
applicazioni scientifiche eseguite su sistemi HPC senza dover analizzare il
codice dell'applicazione stessa ma basandosi solo su una selezione di parametri
di configurazione e sui tempi osservati. Nel presente lavoro si adotta la
medesima strategia, applicata però all'ambito della modellistica numerica
meteorologica.

Non essendo disponibile una storico, è stato necessario creare un dataset
identificando i parametri più rappresentativi, eseguendo il modello secondo
diverse configurazioni e raccogliendo i risultati. Tale raccolta rappresenta
non solo una condizione necessaria per lo scopo del presente lavoro, ma anche
un'utile risorsa per l'area modellistica di Arpae-SIMC nell'analisi delle
prestazioni del modello. Per la definizione dei parametri rilevanti ci si è
avvalsi del supporto di un esperto del dominio.

Nel primo capitolo viene fornita una breve descrizione dei sistemi HPC e dei
modelli meteorologici, in particolare di COSMO e il suo uso presso Arpae-SIMC,
evidenziando le parti di interesse ai fini della realizzazione di strumenti
utili alla previsione del tempo di esecuzione.

Il secondo capitolo è dedicato ai dataset necessari all'addestramento dei
modelli Deep Learning: la definizione dei parametri e dei loro valori, la
selezione delle configurazione da eseguire e infine l'esecuzione dei modelli
per poter effettuare la raccolta dati.

Nel terzo capitolo sono descritti i modelli presi in considerazione per
l'implementazione di predittori dei tempi di esecuzione di COSMO-Model.

Nel quarto capitolo sono elencati e descritti tutti gli esperimenti
effettuati e i confronti tra i vari modelli coinvolti.

\section{Modelli numerici di previsione meteorologica}

I modelli numerici di previsione meteorologica sono modelli matematici che
permettono di prevedere l'andamento delle condizioni atmosferiche in una certa
area del pianeta, simulandone l'andamento attraverso una rappresentazione
schematica della realtà fisica basata su un insieme di equazioni differenziali
alle derivate parziali rispetto al tempo e allo spazio. Tra le equazioni
considerate possiamo citare quella del momento, del primo principio della
termodinamica, di continuità della massa, di stato dei gas, dell'evoluzione del
contenuto liquido e solido delle nubi, dell'evoluzione del vapore acqueo
\cite{Holton2004}.

Non essendo possibile ottenere soluzioni esatte delle equazioni differenziali
alle derivate parziali, si ricorre ad una approssimazione numerica che permette
di ottenere soluzioni discrete, ad esempio mediante il metodo delle differenze
finite. Il compito di risolvere tali equazioni discretizzate
è affidato a programmi specifici, i quali ricevono in ingresso le condizioni
iniziali a partire dalle quali è possibile poi ottenere le previsioni di
numerose variabili meteorologiche per vari istanti di tempo. Questa
discretizzazione comporta la rappresentazione dello spazio e del tempo sotto
forma, rispettivamente, di celle di una griglia tridimensionale (latitudine,
longitudine, quota) e di istanti equidistanti: la semplificazione consiste nel
considerare, dunque, delle condizioni atmosferiche uniformi all'interno di ogni
cella che varia solo nel passaggio da un certo istante ad un altro. Si può
dedurre come dalla dimensione delle celle (il cosiddetto passo di griglia)
dipenda la risoluzione del modello, che è inversamente proporzionale
all'approssimazione spaziale. Tuttavia, una maggiore risoluzione comporta un
maggiore sforzo computazionale e quindi, generalmente, un'alta risoluzione
coincide con uno spazio di applicazione limitato.

Proprio sulla dimensione dell'area possiamo distinguere i modelli in Global
Model (GM) e Limited Area Model (LAM) che, come i nomi suggeriscono, si
occupano di effettuare previsioni su aree di dimensioni diverse. In
particolare, i LAM permettono generalmente di aver previsioni con un maggiore
dettaglio (in cui, ad esempio, i rilievi sono più precisi) ma su un'area
limitata, mentre i GM operano a livello globale basandosi su una orografia
semplificata.

Inoltre, è necessario evidenziare come la lunghezza del passo temporale sia
generalmente nell'ordine di secondi o minuti (in relazione al passo di griglia),
ma i modelli di norma salvano su file solo le soluzioni ottenute a certi istanti di
previsione, tipicamente ogni una o più ore.

\section{High Performance Computing}

Prima di proseguire con la descrizione di COSMO-Model, è opportuno fare una
breve introduzione all'High performance computing (HPC). Tale sistema è
costituito da un insieme di elaboratori (detto cluster) interconnessi via rete
e permette la distribuzione e la parallelizzazione di elaborazioni complesse
attraverso la scomposizione del problema in sottoproblemi eseguiti in parallelo
sui vari nodi, cioè i singoli computer del cluster. Ognuno di questi nodi
lavora quindi in parallelo rispetto agli altri e eventuali sincronizzazioni tra
i sottoproblemi sono tipicamente realizzate mediante scambio di messaggi; a tal
scopo è stato definito Message Passing Interface (MPI), un diffuso standard per
la comunicazione in ambiente HPC.

Il nodo è una macchina fisica all'interno del sistema HPC, quindi composta da
CPU, memoria, rete, etc. Vi possono essere diversi tipi di nodi, che si
differenziano per il loro ruolo: oltre ai nodi dedicati all'elaborazione, ve ne
sono generalmente uno o più configurati come storage per il salvataggio dei
dati e un nodo master necessario per lo scheduling, cioè il controllo,
l'organizzazione e l'assegnazione delle applicazioni (o job) ai vari nodi per
la loro esecuzione. Ogni nodo ha $n_s$ CPU fisiche ognuna delle quali ha $n_c$
core, i quali a loro volta possono eseguire $n_t$ thread: il numero di CPU
logiche è dato da $n_s \times n_c \times n_t$ e rappresenta dunque il numero
massimo di decomposizioni di un singolo job. Ovviamente, l'applicazione di
interesse deve essere progettata e configurabile per poter essere eseguita su
tali sistemi.

Ogni applicazione ha un suo limite intrinseco in termini scalabilità, cioè il
miglioramento delle prestazioni all'aumentare delle risorse (in questo caso
computazionali) assegnate: oltre questa soglia i tempi di esecuzione non
migliorano o addirittura possono peggiorare, in quanto l'aumento della
complessità nella comunicazione tra nodi causa un degrado delle prestazioni.
Anche il numero di core per nodo può influenzare il tempo di esecuzione: oltre
una certa soglia è possibile che la larghezza di banda della memoria non sia
sufficiente e di conseguenza sia più conveniente usare, a parità di core, un
numero maggiore di nodi.

A queste problematiche se ne aggiungono altre, che riguardano ad esempio il
problema di conciliare l'esecuzione parallela di più applicazioni, ognuna delle
quali ha le sue tempistiche e priorità, oppure la gestione di eventuali
fallimenti e recuperi o infine l'individuazione di anomalie nell'esecuzione e
le conseguenti contromisure. Risulta dunque evidente la complessità che sta
alla base della configurazione, schedulazione e esecuzione di una applicazione
su cluster HPC.

\section{COSMO-Model in Arpae-SIMC}

Il modello del Consortium for Small-scale Modeling (COSMO-model)
\cite{Steppeler2003} \cite{COSMO_website}, precedentemente noto con il nome di
Lokal Modell, è un modello di previsione atmosferica su area limitata, oggi
usato non solo per previsioni numeriche meteorologiche, ma anche per numerose
applicazioni scientifiche. Progettato originariamente dal Deutscher
Wetterdienst (DWD, servizio meteo nazionale tedesco), è oggi mantenuto dai
membri di COSMO, di cui fanno parte i servizi meteorologici nazionali di
Italia, Germania, Grecia, Israele, Polonia, Romania, Russia e Svizzera ed enti
di ricerca (tra cui il Centro Italiano Ricerche Aerospaziali e il Centro
Euro-Mediterraneo sui Cambiamenti Climatici), ognuno dei quali gestisce la
propria configurazione del modello.

Data la sua complessità computazionale, il modello è generalmente eseguito in
modalità parallela: ogni processo coinvolto risolve le equazioni del modello
all'interno di un sottodominio regolare, ottenuto dalla decomposizione del
dominio spaziale considerato. I vari processi associati a sottodomini contigui
comunicano mediante scambio di messaggi in quanto necessitano, per i propri
calcoli, dei valori nelle celle posizionate poco oltre i propri confini (figura
\ref{fig:MapSplit}). Di conseguenza, un alto numero di processi comporta una
maggiore parallelizzazione ma anche un maggior numero di messaggi scambiati. La
suddivisione viene fatta solo lungo la latitudine e la longitudine, ottenendo
dunque dei parallelepipedi la cui componente verticale è completamente
contenuta all'interno di un singolo sottodominio: di seguito, possiamo dunque
ignorare quest'ultima dimensione e considerare la proiezione del dominio sul
piano della longitudine e latitudine che convenzionalmente consideriamo da qui
in avanti come un piano cartesiano in cui le due dimensioni geografiche sono
rispettivamente l'asse delle ascisse e delle ordinate.

\begin{figure}[tb]
    \centering
    \includegraphics[width=0.4\textwidth]{images/map-split.png}
    \caption{Suddivisione di un'area in sottodomini e scambio di messaggi tra i
    processi (frecce rosse). Le celle nere rappresentano la discretizzazione
    dello spazio}
    \label{fig:MapSplit}
\end{figure}

A parità di numero di processi e area del dominio, la decomposizione può dare
luogo a sottodomini che hanno dimensione e rapporto tra altezza e larghezza
diversi. Dato un dominio spaziale di $n_i \times n_j$ celle (rispettivamente
lungo la longitudine e latitudine) e scelto il numero di processi $p$, le
decomposizioni possibili sono pari alle coppie di divisori di $p$: infatti, per
ottenere $p$ sottodomini dobbiamo dividere la griglia lungo l'asse $x$ in $n_x$
parti e lungo l'asse $y$ in $n_y$ parti, con $n_x \times n_y = p$. La
dimensione dei sottodomini risultante è $d_x = \frac{n_i}{n_x}$ e $d_y =
\frac{n_j}{n_y}$.

Ad esempio, poniamo di avere un dominio quadrato di 100 x 100 celle e di voler
usare 20 processi (e quindi suddividerlo in altrettanti sottodomini). Le
decomposizioni possibili sono:

\begin{itemize}
    \item $n_x = 1, n_y = 20 \rightarrow d_x = 100, d_y = 5$
    \item $n_x = 2, n_y = 10 \rightarrow d_x = 50, d_y = 10$
    \item $n_x = 4, n_y = 5 \rightarrow d_x = 25, d_y = 20$
    \item $n_x = 5, n_y = 4 \rightarrow d_x = 20, d_y = 25$
    \item $n_x = 10, n_y = 2 \rightarrow d_x = 10, d_y = 50$
    \item $n_x = 20, n_y = 1 \rightarrow d_x = 5, d_y = 100$
\end{itemize}

Non è comunque necessario che la suddivisione sia esatta: ad esempio, poniamo
di decomporre la medesima griglia in 40 sottodomini. Tra le possibili
suddivisioni abbiamo $n_x = 8$ e $n_y = 5$: se la suddivisione lungo l'asse $y$
è esatta ($d_y = 100 \div 5 = 20$), lo stesso non si può dire per l'asse $x$, dove
abbiamo $d_x = \floor{100 \div 8} = 12$ (figura \ref{fig:GridSubdomains}). Questo è
consentito dal modello, che si occuperà di gestire correttamente la mancanza di
celle in alcuni sottodomini.

\begin{figure}[tb]
    \centering
    \includegraphics[width=0.7\textwidth]{images/subdomains.png}
    \caption{Suddivisione di un'area in sottodomini: si può notare come nel primo caso non sia esatta}
    \label{fig:GridSubdomains}
\end{figure}

All'interno del progetto Performance on Massively Parallel Architectures
(POMPA) \cite{POMPA_website}, che analizza le prestazioni del modello COSMO su
architetture HPC, alcuni studi hanno evidenziato come il rapporto tra le
dimensioni $d_x$ e $d_y$ incidano sul tempo di esecuzione: risultati
sperimentali hanno mostrato come si ottengano prestazioni migliori con una forma
più allungata lungo l'asse $x$ (longitudinale) rispetto all'asse $y$
(latitudinale), quindi $d_x \ge d_y$. Questo è dovuto all'organizzazione in
memoria dei dati sotto forma di matrice, che sono contigui lungo l'asse $x$.
Tuttavia, un rapporto $\frac{d_x}{d_y}$ troppo grande equivale a un aumento del
perimetro condiviso tra i sottodomini confinanti che comporta un aumento dello
scambio di messaggi tra processi, vanificando così l'ottimizzazione introdotta
dalla forma del sottodominio.

Nel caso in cui il modello sia eseguito su una architettura cluster, si deve
inoltre decidere come ripartire tali processi tra i nodi e quanti core per nodo
usare: alcuni studi sulle prestazioni del modello COSMO \cite{POMPA_website}
hanno mostrato come a parità di processi ma con diverso numero di nodi (e
conseguentemente core per nodo), l'uso di più nodi (e conseguentemente meno
core per nodo) comporti un miglioramento delle prestazioni. Uno dei colli di
bottiglia maggiori è probabilmente la larghezza di banda della memoria.

Il modello, inoltre, può essere eseguito in singola o doppia precisione, quindi
usando numeri a virgola mobile rispettivamente di 4 e 8 byte. L'uso di singola
precisione comporta un sensibile incremento delle prestazioni, a scapito della
precisione dei valori calcolati.

Nell'ambito modellistico italiano è stato stipulato un accordo denominato
Limited Area Model Italia (LAMI), che vede la collaborazione del Servizio
meteorologico dell'Aeronautica militare, di Arpae-SIMC e di Arpa Piemonte per
lo sviluppo e la gestione operativa delle catene numeriche previsionali
nazionali in Italia. In particolare, Arpae-SIMC gestisce le catene operative
COSMO-5M (previsioni sull'area mediterranea) e COSMO-2I (previsioni
sull'Italia), eseguite sul supercomputer GALILEO di CINECA. Le due
configurazioni si distinguono per la risoluzione, l'area considerata e
l'estensione temporale della previsione.

GALILEO è dotato di 1022 nodi, ognuno dei quali contiene 2 processori Intel
Xeon E5-2697 v4 (Broadwell) a 2.30 GHz da 18 core ciascuno. Inoltre, sono
presenti 60 nodi dotati di 1 GPU nVidia K80 e 2 due nodi con una GPU nVidia
V100. Ogni nodo è dotato di 128 GB di memoria. Arpae-SIMC ha a disposizione 70
dei 1022 nodi con processore Intel.

\begin{figure}[tb]
    \centering
    \includegraphics[width=0.7\textwidth]{images/cosmo-map.png}
    \caption{Dominio di COSMO-5M e COSMO-2I: la rappresentazione non
    rettangolare è dovuta all'uso di un sistema di riferimento ruotato da parte
    del modello}
    \label{fig:CosmoDomain}
\end{figure}

Come è possibile notare in figura \ref{fig:CosmoDomain}, i due modelli usano un
sistema di riferimento ruotato (per necessità legate ai calcoli) all'interno
del quale sono però delle griglie rettangolari. La distinzione tra i due
modelli si basa su quanto è stato precedentemente affermato circa il rapporto
tra risoluzione e estensione del dominio: il modello COSMO-5M ha una
risoluzione minore ma su un dominio e un intervallo temporale più ampio
(tabella \ref{tab:CosmoParams}). Entrambi i modelli sono configurati per
generare una previsione ogni ora.

\begin{table}[h]
    \centering
        \begin{tabularx}{\textwidth}{| X | X | X | X |}
            \hline
            Modello & Dimensione cella & Dominio & Intervallo di previsione \\
            \hline
            COSMO-5M & 5 chilometri & Mediterraneo & previsioni orarie fino a 3 giorni \\
            COSMO-2I & 2 chilometri & Italia & previsioni orarie fino a a 2 giorni \\
            \hline
        \end{tabularx}
        \caption{Versioni operative di COSMO-Model presso Arpae-SIMC}
        \label{tab:CosmoParams}
\end{table}

Queste catene operative sono eseguite due volte al giorno, usando
rispettivamente le ore 00 e 12 come istante iniziale di previsione. Il prodotto
della previsione consiste in una serie di file in formato GRIdded Binary
(GRIB), che viene poi archiviato ed usato come input per la produzione di
prodotti (ad esempio, mappe di previsione) utilizzati quotidianamente per
numerose attività, quali la produzione del bollettino meteo pubblicato sul sito
di Arpae, l'emissione di avvisi meteo da parte del Centro Funzionale regionale
a supporto delle attività della Protezione Civile, le forniture di dati a enti
pubblici e privati.

Proprio per la loro natura e funzione, i modelli devono essere prodotti in
tempi certi e rapidi. Come precedentemente accennato, essendo necessario
partire da dati osservati, il modello ha necessariamente un certo ritardo
rispetto all'istante iniziale di riferimento, dovuto al tempo necessario per
accentrare i dati osservati, generare lo stato iniziale e produrre la
previsione stessa: ad esempio l'esecuzione di mezzanotte di COSMO-5M è
generalmente disponibile verso le quattro di notte. È quindi fondamentale che
il tempo di esecuzione sia minimo, compatibilmente con le risorse disponibili.
